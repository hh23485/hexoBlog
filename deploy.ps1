# powershell

param($msg="update content")

# 部署和推送
hexo clean
hexo g
hexo d

# 提交并推送
git add .
git commit -am "$msg"
git push
