---
title: Nginx 基础 - 01 配置文件和模块
tags:
---


nginx是一个开源且**高性能**、**可靠**的 HTTP 中间件、代理服务。

>Linux IO 包括 `select`、`poll` 和 `epoll` 的三种方式进行。
>1. 对于 `select` 线程轮训，文件句柄上限1024
>2. [`epoll`](https://www.cnblogs.com/ajianbeyourself/p/5859989.html) 模型中采用回调的方式来处理io，由完成 io 的模块通知应用程序完成 io

# 安装

[Nginx官网](http://nginx.org/en/download.html) 提供了各个操作系统完整的安装方式。

直接通过 `apt-get` 安装过程会有一个小问题，就是版本过旧。需要按照如下步骤安装最新的版本：1-添加 `key`，2-添加源，3-更新源，4-`sudo apt-get install nginx` 即可。key 和 源可以在[安装说明](http://nginx.org/en/linux_packages.html#stable)这里找到。


安装完成后，默认的配置目录在`/etc/nginx`文件夹下，以及会自动生成一个默认的日志文件在 `/var/log/nginx` 下。

# 基础配置

nginx 的默认配置文件是 `nginx.conf`，在 nginx 启动的时候，会默认加载 `nginx.conf`，`nginx.conf` 会加载 `conf.d` 下面所有的子配置文件。

## 编译设置

当你输入 `nginx -V` 的时候，可以看到一大串的输出。

``` bash
➜ nginx -V
nginx version: nginx/1.12.2
built by gcc 5.4.0 20160609 (Ubuntu 5.4.0-6ubuntu1~16.04.4)
built with OpenSSL 1.0.2g  1 Mar 2016
TLS SNI support enabled
configure arguments: --prefix=/etc/nginx --sbin-path=/usr/sbin/nginx --modules-path=/usr/lib/nginx/modules --conf-path=/etc/nginx/nginx.conf --error-log-path=/var/log/nginx/error.log --http-log-path=/var/log/nginx/access.log --pid-path=/var/run/nginx.pid --lock-path=/var/run/nginx.lock --http-client-body-temp-path=/var/cache/nginx/client_temp --http-proxy-temp-path=/var/cache/nginx/proxy_temp --http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp --http-scgi-temp-path=/var/cache/nginx/scgi_temp --user=nginx --group=nginx --with-compat --with-file-aio --with-threads --with-http_addition_module --with-http_auth_request_module --with-http_dav_module --with-http_flv_module --with-http_gunzip_module --with-http_gzip_static_module --with-http_mp4_module --with-http_random_index_module --with-http_realip_module --with-http_secure_link_module --with-http_slice_module --with-http_ssl_module --with-http_stub_status_module --with-http_sub_module --with-http_v2_module --with-mail --with-mail_ssl_module --with-stream --with-stream_realip_module --with-stream_ssl_module --with-stream_ssl_preread_module --with-cc-opt='-g -O2 -fstack-protector-strong -Wformat -Werror=format-security -Wp,-D_FORTIFY_SOURCE=2 -fPIC' --with-ld-opt='-Wl,-Bsymbolic-functions -Wl,-z,relro -Wl,-z,now -Wl,--as-needed -pie'
```

这是 nginx 的编译参数，包括了当前版本的 nginx 的各种路径、子模块的情况。你可以看到默认的各种路径比如日志、缓存、配置等等，以及各种子模块的信息，比如 mail http_addition_module等等。这样用于


## 配置文件 conf.d

默认的配置文件是 `nginx.conf`，其中主要关注 http 模块：

``` .d
http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;
    # 定义日志类型
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;
    # 加载所有的子配置文件
    include /etc/nginx/conf.d/*.conf;
}
```

在默认情况下，最后一句会从 `/etc/nginx/conf.d` 加载所有的子配置。`conf.d` 文件夹下包括了了一个 `default.conf`。这是一个代理的样本写法：

``` .d
server {
    listen       80;
    server_name  localhost;

    location / {
        root   /usr/share/nginx/html;
        index  index.html index.htm;
    }
    
    error_page  500 502 503 504 404 /50x.html;
    location = /50x.html {
        root = /usr/share/nginx/html;
    }
    ... 
}
```

`server` 代表提供了一个服务; listen 表示监听的端口; server_name 表示的服务的名称，用于区分不同的服务。

`location` 代表能够处理的路径，一个服务当中可以添加多个location。如果 location 为 `/` 表示如果没有更符合的 localtion 可以处理请求路径时，将会进入 `/` 来处理该请求。

`root` 定义的是首页的路径

`index` 定义的是首页的文件名

`error_page` 定义的是错误页面，在上面的配置中，将会转发到 /50x.html，并配置了 /50x.html 的 root 路径

## 日志配置

nginx 中主要有两种日志类型：

- error.log
- access_log

并且 nginx 中提供了对日志格式的配置方式，虽然只能配置在 http 模块下。

``` .d
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';
                      
    access_log  /var/log/nginx/access.log  main;
```

在配置中可以使用 nginx 自带的变量进行填写：

- HTTP 请求变量 - arg_PARAMETER http_HEADER sent_http_HEADER
- 内置变量
- 自定义变量

其中，Http模块的变量可以参考[http log模块文档](http://nginx.org/en/docs/http/ngx_http_log_module.html)

# 静态资源服务

静态资源服务是 nginx 擅长的


# 代理服务

# 负载均衡

# 动态缓存



