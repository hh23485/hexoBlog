---
title: Java多线程笔记 - 1
date: 2018-02-04
tags: 
    - java
    - 多线程
categories: 
    - 技术
    - 笔记
thumbnail: https://upload-images.jianshu.io/upload_images/1846712-352770cd275396f5.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/800
---

**摘要**

多线程同步的基础资料和讲解在网络上到处都是，从起步的角度来说，包括了如下几个主要的问题：

1. 线程的创建
2. 线程的停止
3. volitale
4. synchronized
5. 可见性
6. 重排序

![image.png](https://upload-images.jianshu.io/upload_images/1846712-352770cd275396f5.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/600)

对于这部分内容，网上有太多写的非常透彻的文章，所以此篇记录下我觉得比较常见和比较详细的一些，之后的部分从同步和锁开始，靠自己总结啦...


<!--more-->


## 死磕Java多线程

## Java多线程源码解析

## 成神之路

## 慕课

## 书

## 面试题





