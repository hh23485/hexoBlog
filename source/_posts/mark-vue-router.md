---
title: vue router官方文档笔记
date: 2017-06-06 09:14:01
tags:
    - vue
categories: 
    - 技术
    - 笔记
---

**摘要**

vue router 是 vue 的路由插件，用于创建单页应用。最初级的使用就是在引用后在router组件中添加一些path，和他们的 name，用来索引路由。但是对于花样布局，路由嵌套等功能，仍然云里雾里，所以记录一下。

按照官方文档的顺序，记录每个章节需要记忆和注意的地方。

![image.png](https://upload-images.jianshu.io/upload_images/1846712-b7df513cc7a6a90b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


<!--more-->

# 基础

## Getting started 开始

使用 vue-router 的一个基本页面如下，里面用到了`router-link`作为单页面路由跳转。在单页面中不能使用`href=`跳转项目内页面，这样会真的跳到新的页面上去了。。。

另外使用了 `router-view` 将匹配的组件渲染在 <router-view/> 中，在这样一种情况下，可以自由的建立 layout，使得需要替换的组件，放在路由视图中。

``` html
<script src="https://unpkg.com/vue/dist/vue.js"></script>
<script src="https://unpkg.com/vue-router/dist/vue-router.js"></script>

<div id="app">
  <h1>Hello App!</h1>
  <p>
    <!-- 使用 router-link 组件来导航. -->
    <!-- 通过传入 `to` 属性指定链接. -->
    <!-- <router-link> 默认会被渲染成一个 `<a>` 标签 -->
    <router-link to="/foo">Go to Foo</router-link>
    <router-link to="/bar">Go to Bar</router-link>
  </p>
  <!-- 路由出口 -->
  <!-- 路由匹配到的组件将渲染在这里 -->
  <router-view></router-view>
</div>
```

### 基础配置

``` js
// 0. 如果使用模块化机制编程，導入Vue和VueRouter，要调用 Vue.use(VueRouter)

// 1. 定义（路由）组件。
// 可以从其他文件 import 进来
const Foo = { template: '<div>foo</div>' }
const Bar = { template: '<div>bar</div>' }

// 2. 定义路由
// 每个路由应该映射一个组件。 其中"component" 可以是
// 通过 Vue.extend() 创建的组件构造器，
// 或者，只是一个组件配置对象。
// 我们晚点再讨论嵌套路由。
const routes = [
  { path: '/foo', component: Foo },
  { path: '/bar', component: Bar }
]

// 3. 创建 router 实例，然后传 `routes` 配置
// 你还可以传别的配置参数, 不过先这么简单着吧。
const router = new VueRouter({
  routes // （缩写）相当于 routes: routes
})

// 4. 创建和挂载根实例。
// 记得要通过 router 配置参数注入路由，
// 从而让整个应用都有路由功能
const app = new Vue({
  router
}).$mount('#app')
```

## 路由动态匹配

### 路径参数

vue-router提供了类似restful风格的路由匹配

例如：

``` js
const User = {
  template: '<div>User</div>'
}

const router = new VueRouter({
  routes: [
    // 动态路径参数 以冒号开头
    { path: '/user/:id', component: User }
  ]
})
```

像 `/user/foo` 和 `/user/bar`都将映射到相同的路由上。该参数会放在路由对象中，以`this.$route.params.id`这样的形式可以访问，`this`必须在组件内使用。

``` js
const User = {
  template: '<div>User {{ $route.params.id }}</div>'
}
```

![](http://oiqdakvix.bkt.clouddn.com/14967169929327.jpg)

### 响应参数变化

如果从`/user/foo`跳转到`user/bar`，原来的组件实例不会被删除，而是重新渲染一次。这意味着**路由钩子**不会被再次调用（例如登陆检查。。），因此同一个路由匹配的跳转，需要通过`watch`进行监测，监测也是一个钩子方法，用例如下：

``` js
const User = {
  template: '...',
  watch: {
    '$route' (to, from) {
      // 对路由变化作出响应...
    }
  }
}
```

### 匹配优先级

优先级就是书写顺序。。。

### 正则匹配

规则比较多，最好直接查看[官方文档](https://github.com/pillarjs/path-to-regexp#parameters)，以及[官方实例](https://github.com/vuejs/vue-router/blob/next/examples/route-matching/app.js)


## 嵌套路由

组件是通过路由匹配的，因此在组件嵌套的时候，也可以通过路由嵌套完成。例如通过`children`，指定子组件和路径，在组件内部通过`<router-view></router-view>`进行渲染。例如：

如果需要在User组件中添加Profile或者Posts子组件

![](http://oiqdakvix.bkt.clouddn.com/14967183301916.jpg)

借助`vue-router`实现如下：

``` js
<div id="app">
  <router-view></router-view>
  <!-- 顶层路由 用于渲染User组件-->
</div>
```

```js
const User = {
  template: '<div>User {{ $route.params.id }}</div>'
}

const router = new VueRouter({
  routes: [
    { path: '/user/:id', component: User }
  ]
})

const User = {
  template: `
    <div class="user">
      <h2>User {{ $route.params.id }}</h2>
      <!-- 中间路由 用于渲染子组件 -->
      <router-view></router-view>
    </div>
  `
}

const router = new VueRouter({
  routes: [
    { path: '/user/:id', component: User,
      children: [
        {
          // 当 /user/:id/profile 匹配成功，
          // UserProfile 会被渲染在 User 的 <router-view> 中
          path: 'profile',
          component: UserProfile
        },
        {
          // 当 /user/:id/posts 匹配成功
          // UserPosts 会被渲染在 User 的 <router-view> 中
          path: 'posts',
          component: UserPosts
        }
      ]
    }
  ]
})
```

**需要注意的**

1. 在子路由的开始不可以添加/，不然会被当做根路径**
2. 在上述配置方式上，/user/foo/profile会被匹配，但/user/foo上则不会匹配到路由，因此添加**空的**子路由可以解决

``` js
const router = new VueRouter({
  routes: [
    {
      path: '/user/:id', component: User,
      children: [
        // 当 /user/:id 匹配成功，
        // UserHome 会被渲染在 User 的 <router-view> 中
        { path: '', component: UserHome },

        // ...其他子路由
      ]
    }
  ]
})
```

[可运行实例](http://jsfiddle.net/yyx990803/L7hscd8h/)

## 编程式导航

### router.push

编程式和声名式是两种方法，就像spring的配置一样。。。你是声名在xml中，还是通过java配置。
在html部分（template）中，使用声明式，但是在js脚本中，使用编程式的写法。

| 声明式 | 编程式 |
| --- | --- |
| <router-link :to='…'> | router.push(…) |

方法如下：

```js
// 字符串
router.push('home')

// 对象
router.push({ path: 'home' })

// 命名的路由
router.push({ name: 'user', params: { userId: 123 }})

// 带查询参数，变成 /register?plan=private
router.push({ path: 'register', query: { plan: 'private' }})
```

### router.replace

| 声明式 | 编程式 |
| --- | --- |
| <router-link :to='…'> | router.push(…) |

replace与push很像，但是push可见是一个stack，可以push，自然也能后退。但是事实上在replace操作下，只是将当前的路由替换了，虽然页面跳转了，但是并不能记录在历史中。

### router.go(n)

表示在记录中移动，例如

```
// 在浏览器记录中前进一步，等同于 history.forward()
router.go(1)

// 后退一步记录，等同于 history.back()
router.go(-1)

// 前进 3 步记录
router.go(3)

// 如果 history 记录不够用，那就默默地失败呗
router.go(-100)
router.go(100)
```

### 路由命名

有的时候，在使用路径跳转的时候，有点慌，需要来回确认，因此可以使用命名进行跳转会方便很多。

``` js
const router = new VueRouter({
  routes: [
    {
      path: '/user/:userId',
      name: 'user',
      component: User
    }
  ]
})
```

命名方式如上所示，通过跳转的时候使用name参数指定跳转的位置。

```js
<!--编程式-->
router.push({ name: 'user', params: { userId: 123 }})
<!--声明式-->
<router-link :to="{ name: 'user', params: { userId: 123 }}">User</router-link>
```

### 命名视图

同级显示多个视图，通过<router-view/>自动分辨是很难的，因此需要使用视图的命名，实例如下：

``` js
<router-view class="view one"></router-view>
<router-view class="view two" name="a"></router-view>
<router-view class="view three" name="b"></router-view>

const router = new VueRouter({
  routes: [
    {
      path: '/',
      components: {
        default: Foo,
        a: Bar,
        b: Baz
      }
    }
  ]
})
```

### 重定向

重定向是在路由配置的时候使用redirect参数，有如下几种使用方法：

**直接跳转：**

``` js
const router = new VueRouter({
  routes: [
    { path: '/a', redirect: '/b' }
  ]
})
```


**name跳转**

``` js
const router = new VueRouter({
  routes: [
    { path: '/a', redirect: { name: 'foo' }}
  ]
})
```

**回调**

``` js
const router = new VueRouter({
  routes: [
    { path: '/a', redirect: to => {
      // 方法接收 目标路由 作为参数
      // return 重定向的 字符串路径/路径对象
    }}
  ]
})
```

### 别名

下面一段来自官方文档：


>『重定向』的意思是，当用户访问 /a时，URL 将会被替换成 /b，然后匹配路由为 /b，那么『别名』又是什么呢？
/a 的别名是 /b，意味着，当用户访问 /b 时，URL 会保持为 /b，但是路由匹配则为 /a，就像用户访问 /a 一样。
上面对应的路由配置为：

```
const router = new VueRouter({
  routes: [
    { path: '/a', component: A, alias: '/b' }
  ]
})
```

>『别名』的功能让你可以自由地将 UI 结构映射到任意的 URL，而不是受限于配置的嵌套路由结构。
更多高级用法，请查看 [例子](https://github.com/vuejs/vue-router/blob/next/examples/route-alias/app.js).

## history

history模式保证url能够跟正常的url显示的一样，但是由于路由匹配问题，如果没有访问到正确的路由，可能会返回404，因此需要后台捕捉404错误返回错误页面，或者前台响应所有的请求，默认覆盖到404页面。

``` js
const router = new VueRouter({
  mode: 'history',
  routes: [
    <!--由于优先级，前面需要放置其他路由-->
    { path: '*', component: NotFoundComponent }
  ]
})
```

# 进阶

## 导航钩子

导航钩子是拦截导航的方式，能够在进入路由之前、之后触发，用来做过滤、预操作等等。

在vue-router中提供了三种钩子供使用：

1. 全局的
2. 单个路由独享的
3. 组件级的

### 全局

全局是通过router对象来遍历所有的路由对象添加钩子，通过异步解析执行。

```js
const router = new VueRouter({ ... })

router.beforeEach((to, from, next) => {
  // ...
})
```

钩子提供了三个参数：

- to: Route: 即将要进入目标的[路由对象](https://router.vuejs.org/zh-cn/api/route-object.html)
- from: Route: 当前导航正要离开的路由
- next: function: 类似resolve，表示完成当前钩子。有三种参数形式。
    - next(): 表示resolve，使导航的状态辩位confirmed。
    - next(false): 中断当前导航，重置到from的路由地址。
    - next(path): 跳转到一个不同的地址，终止当前的导航。

一定要在整个钩子的过程中调用next方法，不然钩子不会被resolved。

而`afterEach(route=>{})`表示完成导航后的钩子方法。

### 路由独享的钩子

在路由配置上可以直接定义`beforeEnter`钩子，参数与全局的一致。

```js
const router = new VueRouter({
  routes: [
    {
      path: '/foo',
      component: Foo,
      beforeEnter: (to, from, next) => {
        // ...
      }
    }
  ]
})
```


### 组件级钩子

另外还有组件内钩子，组件内钩子直接在component里面定义。

总共存在三种组件内钩子：

- beforeRouteEnter
- beforeRouteUpdate (version>2.2)
- beforeRouteLeave

```js
const Foo = {
  template: `...`,
  beforeRouteEnter (to, from, next) {
    // 在渲染该组件的对应路由被 confirm 前调用
    // 不！能！获取组件实例 `this`
    // 因为当钩子执行前，组件实例还没被创建
    // next(vm => {
    //     // 通过 `vm` 访问组件实例
    // })
  },
  beforeRouteUpdate (to, from, next) {
    // 在当前路由改变，但是该组件被复用时调用
    // 举例来说，对于一个带有动态参数的路径 /foo/:id，在 /foo/1 和 /foo/2 之间跳转的时候，
    // 由于会渲染同样的 Foo 组件，因此组件实例会被复用。而这个钩子就会在这个情况下被调用。
    // 可以访问组件实例 `this`
  },
  beforeRouteLeave (to, from, next) {
    // 导航离开该组件的对应路由时调用
    // 可以访问组件实例 `this`
  }
}
```

leave钩子可以禁止用户在还未保存修改的时候突然离开，通过next(false)来取消导航。即通过一个变量可以check是不是内容修改了没有保存，在leave钩子里检查，并提示用户处理。

## 路由元信息

定义路由的时候可以配置meta字段，字段当中可以存放一些变量，在钩子中、或者组件中都可以访问。

在钩子中访问就是通过`to`来访问，而在组件内则通过`this.$route$`来获取。可以通过遍历`$route.matched`来检查记录中的meta字段。

```js
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!auth.loggedIn()) {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next() // 确保一定要调用 next()
  }
})
```

因此多项检查的时候，可以在meta中定义需要检查的项，接连检查。

不过个人使用Promise来多重验证。。。在最后一项检查的时候，使用next()跳出，除此以外都是用promise.resolve进入下一步检查。

## 过渡特效

就是<transition>。。。

不过有一个很高级的用法：

**根据路由路径的长度来确定向左还是向右，当然你也可以使用更高级的判断方式**

``` js
<!-- 使用动态的 transition name -->
<transition :name="transitionName">
  <router-view></router-view>
</transition>
// 接着在父组件内
// watch $route 决定使用哪种过渡
watch: {
  '$route' (to, from) {
    const toDepth = to.path.split('/').length
    const fromDepth = from.path.split('/').length
    this.transitionName = toDepth < fromDepth ? 'slide-right' : 'slide-left'
  }
}

```


## 路由懒加载

将不同的路由组件切割成不同的代码块，能够减少js包的大小，加速页面加载，访问的时候才会去加载对应的模块，这样会更高效。

一般写法：

```js
const Foo = resolve => {
  // require.ensure 是 Webpack 的特殊语法，用来设置 code-split point
  // （代码分块）
  require.ensure(['./Foo.vue'], () => {
    resolve(require('./Foo.vue'))
  })
}
```


AMD风格的require，在引入的时候就按照这样的方式引入组件.

```js
const Foo = resolve => require(['./Foo.vue'], resolve)
```

也可以分组，将某个路由下的所有组件打包到同一个异步的chunk中，只需要给chunk命名即可。

```js
const Foo = r => require.ensure([], () => r(require('./Foo.vue')), 'group-foo')
const Bar = r => require.ensure([], () => r(require('./Bar.vue')), 'group-foo')
const Baz = r => require.ensure([], () => r(require('./Baz.vue')), 'group-foo')
```





