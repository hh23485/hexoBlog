---
title: ubuntu 装机 all in one
date: 2018-02-14
tags: 
    - ubuntu
categories: 
    - 装机
    - 系统
---

**ubuntu 16.04 装机脚本**

* [x] 创建用户
* [x] 分配权限
* [x] 添加远程访问
* [x] 添加ssh免密访问
* [x] 将bash修改为zsh
* [x] zsh推荐主题
* [x] 脚本安装vim并添加spf13
* [x] 安装tmux
* [x] 安装jdk
* [x] 安装docker
* [x] 安装mysql
* [x] 安装nginx

不需要再一个个找安装配置博客，All in One

<!-- more -->

# 系统用户

## 创建用户

``` bash
# 创建用户
sudo adduser username

# 设置密码
sudo passwd username
```

## 分配权限

在 `/etc/sudoers` 文件中将username的权限信息添加到 `User privilege specification` 下：

``` bash
sudo vim /etc/sudoers
# 添加到下面位置
root ALL=(ALL) ALL
username ALL=(ALL) ALL
```

## 添加远程访问

``` bash
ssh username@host
# 输入密码
```

如果想要 ssh 免密登陆，需要填写本地的公钥至服务器的 `authorized_keys` 文件中。

如果没有生成ssh密钥请先输入 `ssh-keygen` 生成。

可以通过下列命令查看本地机器的 ssh 公钥，如果没有找到该文件，则可能没有生成密钥，如果生成成功，下面的命令将会输出公钥。

``` bash
cat ~/.ssh/id_rsa.pub
```

将输出的公钥拷贝至服务器 `~/.ssh/authorized_keys` 文件中，如果没有请自己创建。

## 设置断线时间延长

找到 `/etc/ssh/sshd_config` 配置文件，增加或修改一条设置

``` shell
ClientAliveInterval 60
```

输入下面的命令重启服务令设置生效

``` bash
service sshd reload
```

# apt源

更新apt，保证软件能够正常更新

```
sudo apt-get update
```

添加源方法和源地址，查看 [ubuntu的软件源更换 - CSDN博客](http://blog.csdn.net/wuzuodingfeng/article/details/76155829)

# Bash -> Zsh

## 安装及启用

``` shell
# 安装
sudo apt-get install zsh -y

# 安装 oh my zsh
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# 更换启动 sh
sudo chsh username -s /bin/zsh
```

## 主题推荐

``` bash
vi ~/.oh-my-zsh/themes/robbyrussell.zsh-theme
```

将文件中内容替换为：

```
local ret_status="%(?:%{$fg_bold[green]%}➜ :%{$fg_bold[red]%}➜ %s)"
#配置
PROMPT='%{$fg_bold[red]%}➜ %{$fg_no_bold[green]%}%p%{$fg[green]%}%d %{$fg_no_bold[cyan]%}$(git_prompt_info)%{$fg_bold[cyan]%}% %{$fg_bold[green]%}> %{$reset_color%}'
ZSH_THEME_GIT_PROMPT_PREFIX="git:(%{$fg_bold[red]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg_no_bold[cyan]%}) %{$fg_no_bold[yellow]%}✗ %{$reset_color%}"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg_no_bold[cyan]%}) "
```

设置了彩色的全路径 以及对 git 状态的检测。

![image.png](https://upload-images.jianshu.io/upload_images/1846712-f231bb044c89e516.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

## 配置命令自动补全 incr

下载incr并放置到插件文件夹中：

```
# 创建插件文件夹
mkdir ~/.oh-my-zsh/plugins/incr

# 下载插件
wget -P ~/.oh-my-zsh/plugins/incr/ http://mimosa-pudica.net/src/incr-0.2.zsh

# 追加启动生效
echo source ~/.oh-my-zsh/plugins/incr/incr*.zsh >> ~/.zshrc
```

![image.png](https://upload-images.jianshu.io/upload_images/1846712-4a0a8be74af326e6.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

# vim -> vim + lua

之前写过一篇[安装 vim + lua 并添加 spf13](http://blog.hhchat.cn/2017/10/05/let-ubuntu-use-vim-lua-spf13/)，讲了一些详细的步骤，不过你可以使用下面的脚本执行编译安装和配置。

``` bash
# 获取所有的老版本
echo '[✔] 开始安装 vim '

# 安装依赖
sudo apt-get install -y libncurses5-dev libgnome2-dev libgnomeui-dev \
    libgtk2.0-dev libatk1.0-dev libbonoboui2-dev \
    libcairo2-dev libx11-dev libxpm-dev libxt-dev python-dev \
    python3-dev ruby-dev liblua5.1 lua5.1-dev libperl-dev git
echo '[✔] 依赖安装完成'

# 下载特定版本的vim
mkdir -p ~/download
echo '[✔] 创建下载文件夹 '

wget -P ~/download/ https://codeload.github.com/vim/vim/zip/v8.0.1438
echo '[✔] vim v8.0.1438 下载完成'

# 解压vim
cd ~/download && unzip ~/download/v8.0.1438
echo '[✔] 解压完成'


# 进入vim 配置
cd ~/download/vim-8.0.1438 && ./configure --with-features=huge \
            --enable-multibyte \
            --enable-rubyinterp=yes \
            --enable-pythoninterp=yes \
            --with-python-config-dir=/usr/lib/python2.7/config \
            --enable-python3interp=yes \
            --with-python3-config-dir=/usr/lib/python3.4/config \
            --enable-perlinterp=yes \
            --enable-luainterp=yes \
            --enable-gui=gtk2 --enable-cscope --prefix=/usr

# 安装 vim
cd ~/download/vim-8.0.1438 && make VIMRUNTIMEDIR=/usr/share/vim/vim80 
cd ~/download/vim-8.0.1438 && sudo make install
echo '[✔] 安装完成'

# 执行 spf13
curl https://j.mp/spf13-vim3 -L > spf13-vim.sh && sh spf13-vim.sh
echo '[✔] 安装spf13完成'

# 提示完成
echo '[✔] VIM安装完成'

```

在完成后，需要在 bash 或者 zsh 中设置两个别名：

- `alias sudo="sudo "`
- `alias vi="vim"`

注意第一条后面双引号中间的空格，这是核心，能够避免通过 `sudo vi` 的时候的启动错误。

# tmux

使用 apt-get 下载和安装

``` shell
sudo apt-get install -y tmux
```

tmux 使用教程可以查看 [Tmux 速成教程：技巧和调整](http://blog.jobbole.com/87584/)，配置可以参考另外一篇[博客](http://blog.hhchat.cn/2018/02/15/config-and-use-of-tmux-in-ubuntu/)。

# jdk

1. 去 [Oracle官网](http://www.oracle.com/technetwork/java/javase/overview/index.html) 下载jdk，解压后放置在 `~/lib` 文件夹下。
2. 如果你使用 zsh，在 `~/.zshrc` 中添加 `JAVA_HOME` 为jdk路径，并添加 PATH 变量为 `PATH=$PATH:$JAVA_HOME/path`
3. 输入 `source ~/.zhsrc` **令其生效**
3. 在终端输入 `java -version` 查看 jdk 版本

[使用 wget 下载 JDK 时需要小心 5k 的问题](http://blog.hhchat.cn/2018/02/13/use-wget-to-download-jdk-from-oracle/)


`~/.zsrhc` 中配置 `JAVA_HOME` 和 `PATH` 如下：

![image.png](https://upload-images.jianshu.io/upload_images/1846712-8f239f60b2eecbed.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

# mysql

## 安装

通过 apt-get 下载安装

``` bash
sudo apt-get install -y mysql-server 
```

## 添加一个用户并授权

``` mysql
# 创建用户
CREATE USER 'username'@'host' IDENTIFIED BY 'password'; 

# 授权远程访问
GRANT ALL PRIVILEGES ON *.* TO 'username'@'%' IDENTIFIED BY 'password' WITH GRANT OPTION;

# 修改默认密码
ALTER USER 'root'@'localhost' IDENTIFIED BY 'password'; 

# 修改密码
set password for 'root'@'localhost'=password('password');
```


## 开放远程访问

``` shell
sudo vi /etc/mysql/mysql.conf.d/mysqld.cnf
```

找到

`# bind-address            = 127.0.0.1`

前面加#注释掉，`wq!`强制保存并退出，重启服务。

## 重启服务

``` bash
service mysql restart
```

## 进入

``` bash
mysql -u root -p
# 输入你的密码
```

# docker

你可以去[官方](https://www.notion.so/hh23485/a2a9ce22557e441faad4b3bb0c33f038#4bc8ee5a79d0449fa6fa79b6e3385bce)查看安装步骤或安装指定版本，也可以使用下面的脚本安装最新的版本。

``` bash
# 卸载旧版本
sudo apt-get remove docker docker-engine docker.io

# 刷新源
sudo apt-get update

# 安装依赖包
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common

# 添加gpg key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# 添加软件包
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

# 更新软件源信息
sudo apt-get update

# 安装docker
sudo apt-get install -y docker-ce

# 配置加速器
curl -sSL https://get.daocloud.io/daotools/set_mirror.sh | sh -s http://a06e03e4.m.daocloud.io

# 重启docker服务
sudo service docker restart

# 运行 hello world
sudo docker run hello-world

echo '[✔] docker-se 安装完成'

```

脚本中配置的 Docker 加速器来自 [DaoCloud](http://www.daocloud.io/mirror#accelerator-doc)

# nginx

通过 apt-get 下载安装

``` bash
sudo apt-get install -y nginx
```

同时，在 nginx 的配置文件的根目录在 `/etc/nginx`

![image.png](https://upload-images.jianshu.io/upload_images/1846712-00cbd480067b07c6.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


# the fuck


[github fuck 官方主页](https://github.com/nvbn/thefuck)

安装

``` bash
sudo apt update
sudo apt install python3-dev python3-pip
sudo pip3 install thefuck

eval $(thefuck --alias) >> ~/.zshrc
source ~/.zshrc
```

更新

``` bash
pip3 install thefuck --upgrade
```

