---
title: Mybatis Generateor 配置模板
date: 2018-02-08 11:37:14
tags: 
    - mybaits
categories: 
    - 技术
    - 配置
---

之前一直在使用 Hibernate 和 JPA，但是为了找工作不得不强行上Mybatis。为了避免每次都到处找配置，还是老方法记录一份模板。

![image.png](https://upload-images.jianshu.io/upload_images/1846712-7007a012fce3daad.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

<!-- more -->

首先是 Mybatis generator 依赖库插件，放在 `pom` 的 `build/plugins` 标签下

``` xml
<plugin>
    <groupId>org.mybatis.generator</groupId>
    <artifactId>mybatis-generator-maven-plugin</artifactId>
    <version>1.3.2</version>
    <configuration>
        <verbose>true</verbose>
        <overwrite>true</overwrite>
    </configuration>
</plugin>
```

然后添加 `generatorConfig.xml`

``` xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE generatorConfiguration PUBLIC
        "-//mybatis.org//DTD MyBatis Generator Configuration 1.0//EN"
        "http://mybatis.org/dtd/mybatis-generator-config_1_0.dtd" >
<generatorConfiguration>

    <properties resource="datasource.properties"/>
    <!-- !!!! Driver Class Path !!!! 目前最好使用5.1.6版本的驱动，比较稳定 -->
    <classPathEntry location="***********\.m2\repository\mysql\mysql-connector-java\5.1.6\mysql-connector-java-5.1.6.jar"/>

    <context id="context" targetRuntime="MyBatis3">
        <commentGenerator>
            <property name="suppressAllComments" value="false"/>
            <property name="suppressDate" value="true"/>
        </commentGenerator>

        <!-- !!!! Database Configurations !!!! -->
        <jdbcConnection driverClass="${db.driverClassName}" connectionURL="${db.url}" userId="${db.username}" password="${db.password}"/>

        <javaTypeResolver>
            <property name="forceBigDecimals" value="false"/>
        </javaTypeResolver>

        <!-- !!!! Model Configurations !!!! -->
        <javaModelGenerator targetPackage="path.to.pojo.pkg" targetProject="./src/main/java">
            <property name="enableSubPackages" value="false"/>
            <property name="constructorBased" value="true"/>
            <property name="trimStrings" value="true"/>
            <property name="immutable" value="false"/>
        </javaModelGenerator>

        <!-- !!!! Mapper XML Configurations !!!! -->
        <sqlMapGenerator targetPackage="mappers" targetProject="./src/main/resources">
            <property name="enableSubPackages" value="false"/>
        </sqlMapGenerator>

        <!-- !!!! Mapper Interface Configurations !!!! -->
        <javaClientGenerator targetPackage="path.to.dao.pkg" targetProject="./src/main/java" type="XMLMAPPER">
            <property name="enableSubPackages" value="false"/>
        </javaClientGenerator>

        <!-- !!!! Table Configurations !!!! -->
        <table tableName="tableName" domainObjectName="objectName" enableCountByExample="false" enableUpdateByExample="false" enableDeleteByExample="false" enableSelectByExample="false" selectByExampleQueryId="false"/>
        <table tableName="yaTableName" domainObjectName="yaObjectName" enableCountByExample="false" enableUpdateByExample="false" enableDeleteByExample="false" enableSelectByExample="false" selectByExampleQueryId="false">
            <columnOverride column="someColumn1" jdbcType="VARCHAR" />
            <columnOverride column="someColumn2" jdbcType="VARCHAR" />
        </table>
    </context>
</generatorConfiguration>
```

