---
title: 使用Linux共享打印机
date: 2017-09-28
tags: 
    - linux
    - cups
categories: 
    - 装机
    - 打印机
---

**摘要**

实验室一直使用Windows的服务器来共享打印机，但是由于windows的。。。。开发环境比较恶劣，终于我们也换成了ubuntu来支撑日常的工作。

在所有开发之前遇到的问题就是，我们已经在Linux上安装了打印机的驱动，但其他局域网内的计算机不知道怎么样连接到打印机了。。。

在查询了一早上的资料后，发现在Linux上共享打印机比之前要更简单一些，步骤如下：

1. 安装`cups`服务
2. 启动`cups`服务
3. 进去基于浏览器或者文档的配置环境
4. 添加打印机或者直接复制出默认配置的打印机地址
5. 在配置中勾选共享
6. 重新启动`cups`服务

<!-- more -->

下面一步一步来说：

### 安装cups服务

在ubuntu上安装cups服务和安装其他组件没有什么区别，一条命令，毫无压力

```
sudo apt-get install cups
```

在安装的时候遇到了依赖无法解决的问题，但是提示说使用`--fix-missing`选项来`update`，那就试一下

```
sudo apt-get update --fix-missing
sudo apt-get install cups
```

完成~

### 启动cups服务

```
service cups start
```

### 配置打印机

cups在浏览器中提供了一个gui的控制页面，如果你的ubuntu是有图形化界面的，那你可以直接使用`http://localhost:631`进入管理页面，如果你的ubuntu是服务器版本的，那就从局域网中输入ip进入吧。

页面长成这个样子。


![管理页面](https://upload-images.jianshu.io/upload_images/1846712-2f7d339a5830d617.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

其实这些内容已经非常明白了，上面是一个导航，你可以进入不同的菜单。这里主要关注的是两个地方，一个是导航栏中的`Printers`，另一个是`Adding Printers and Classes`。首先查看Printers中有没有你想要共享的打印机，如果有的话，可以跳过这一步。如果没有的话，你需要进入`Adding Printers and Classes`来选择你的打印机并添加，一路选择默认即可。

这里需要注意的是，我已经在共享之前配置好了本地的打印机设置和驱动，也就是利用ubuntu自带的打印机管理就可以，因此这里没有什么技术障碍，就略过了。

### 获取打印机连接地址

在cups中共享的打印机有统一的链接地址，首先，进入导航栏的`Printers`页面中，页面中可以看到你刚才添加或默认添加的打印机。


![打印机列表](https://upload-images.jianshu.io/upload_images/1846712-2e72c44843e209cc.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

点击你要共享的打印机项，你可以进入一个详情，详情上的url就是远程访问的打印机地址。


![打印机地址](https://upload-images.jianshu.io/upload_images/1846712-162384b5f31f0753.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

当然，如果你的地址是`localhost`，只需要转换成对应的区域网ip即可访问。

### 共享打印机

cups默认是不会共享打印机的，因此你需要在导航栏的Administration中做一点操作。


![共享打印机](https://upload-images.jianshu.io/upload_images/1846712-812300534625603a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

在`Administration`页面中，勾选`Share printers connected to this system`和`Allow printing from the Internet`选项并点击`Change Settings`保存配置。

### 大功告成

这个时候，你在其他计算机上已经可以访问该打印机了，只需要输入URL即可搜索到该打印机。


![添加打印机](https://upload-images.jianshu.io/upload_images/1846712-62ee2abd3a96d1aa.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

然后选择对应的驱动，确认，完成。


![选择打印机驱动](https://upload-images.jianshu.io/upload_images/1846712-b4bce9eefb0ca660.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


现在已经可以开始打印了~

