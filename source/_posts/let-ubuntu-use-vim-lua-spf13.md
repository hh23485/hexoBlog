---
title: vim 添加 lua 并配置 spf13
date: 2017-10-05
tags: 
    - vim
    - ubuntu
categories: 
    - 技术
    - 配置
---

**摘要**

vim 在 Linux 下算是非常强大的编辑器，但是对于我这种从 ide 转到 Linux 下的孩子，白板编程的效率低到令人发指。

因此，你需要对vim进行一些改造，我曾经问过一些人怎么样改造vim，让他能够更适合作为一个写代码的工具（不是IDE）。他们说，不要过度的折腾，找一个通用的版本配置一下就可以了。

在之后，我发现了一个叫做[spf13-vim](https://github.com/spf13/spf13-vim)的github开源项目，这个项目是一个vim配置的合集，包含了配色、快捷键、插件等等一系列的内容。具体的使用方式可以参考[spf13系列博文](http://blog.csdn.net/dark_tone/article/details/52879487)。这里主要记录安装的所有命令 - - 毕竟使用可以直接成为习惯，而安装的过程怎么都记不住。。。

<!--more-->

***
本篇适用于 `ubuntu`，`centos` 可以参考，但是依赖包名称不同，需要自己匹配。

在开始之前，需要确认一下vim的版本，这也是本篇的重点之一。进入vim，输入 `:version`会看到vim当前版本的信息：

![:version](https://upload-images.jianshu.io/upload_images/1846712-6ea3faff160d70d6.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

在该版本中，可以发现一个重要的内容，在第二列中，标注了`-lua`，这表示了当前的vim并不支持lua，而`spf13`中的部分插件，例如自动补全是依赖于lua的，因此当前的vim版本没有办法支撑spf13的全部功能。所以，我们需要重新编译vim并替换系统默认的vim来添加lua的支持。

***

# 重新编译vim80 添加lua支持

这里其实有一篇[文章](http://blog.csdn.net/keith_bb/article/details/69788343)已经讲的很细，但是有些地方还是不太适应我的环境，因此我稍微改动一下，并整理到一起。


## 1. 检查依赖

首先你需要确认你的系统环境和依赖情况，如果你在ubuntu上，那么你可以按照下面的命令输入一波，这样就会安装所有需要的依赖。当然如果你在centOS上，那你自己转换一下对应的yum包名称吧，或者，直接下一步，直到出错提示你安装依赖。（**git是必须的**）

``` shell
sudo apt-get install libncurses5-dev libgnome2-dev libgnomeui-dev \
    libgtk2.0-dev libatk1.0-dev libbonoboui2-dev \
    libcairo2-dev libx11-dev libxpm-dev libxt-dev python-dev \
    python3-dev ruby-dev liblua5.1 lua5.1-dev libperl-dev git
```

***

## 2. 下载git源码 (git clone)

在添加完成依赖之后，可以开始下载vim了。网上vim的路径千千万，为了避免出现尴尬的错误，还是直接从官方git仓库下载。输入下列命令克隆git仓库master分支源码到当前文件夹。

``` shell
//输入git克隆命令下载master分支的源码到当前文件夹
git clone https://github.com/vim/vim.git
```

在输入完成后，你需要静静的等待下载完成，当然你不能关掉终端，否则就会停掉。


![git clone](https://upload-images.jianshu.io/upload_images/1846712-0442fe22f701f8e8.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

这个仓库的下载速度一直都是非常迷的。。。不知道国内有没有镜像。

***

## 3. 删除原有的vim和vi相关的库

我之前在重新编译添加了新的vim后，曾经出现过一个比较严重的错误。当我使用vim进行编辑的时候一切正常，但是如果我使用`sudo vi`进行编辑，那么就会各种报错，貌似这和`sudo`的原理有关系。我修改了`root`用户的权限和默认编辑器一直都没有解决。

好在后来找到了解决办法：

1. 卸载原有的vi再编译新的vim
2. 添加新的alias在`.zshrc`或者`.bashrc`中，`alias sudo='sudo '`，注意最后的空格


**注意**：如果你要删除vim，请确保linux里有其他可以编辑的环境，比如gedit或者nano等等。
卸载的办法比较直接：

```
//先查看已经安装的vim相关内容
dpkg -l | grep vim
//全部一个个卸载掉
sudo apt-get remove vim-common vim-tiny vim vim-runtime
```

![已经安装的vim相关应用](https://upload-images.jianshu.io/upload_images/1846712-0c5ba5b279c5f32b.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

这个时候，你的vim命令已经不起作用了。如果你已经下载好了上一步中的源码，那么下面可以开始编译了。

***

## 4. 编译vim80

vim编译的命令是从前面引用的博客里面看到的，不过这里的python需要根据个人环境修改一下，比如我这里的环境是python2.7/3.4，对应的位置做了一点修改。

```
cd vim
./configure --with-features=huge \
            --enable-multibyte \
            --enable-rubyinterp=yes \
            --enable-pythoninterp=yes \
            --with-python-config-dir=/usr/lib/python2.7/config \
            --enable-python3interp=yes \
            --with-python3-config-dir=/usr/lib/python3.4/config \
            --enable-perlinterp=yes \
            --enable-luainterp=yes \
            --enable-gui=gtk2 --enable-cscope --prefix=/usr
make VIMRUNTIMEDIR=/usr/share/vim/vim80 
sudo make install
```

安装完成之后，可以在.zshrc中配置一下`alias vi='vim'`就可以愉快的使用了，当然你也可以再次输入`:version`看一下版本是不是正确的。


![:version](https://upload-images.jianshu.io/upload_images/1846712-372331bde6c9bedb.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


***

# 安装spf13

spf13的github主页提供了一个一键安装的脚本，有两种命令可选：

基于curl：

```
curl https://j.mp/spf13-vim3 -L > spf13-vim.sh && sh spf13-vim.sh
```

直接用于bash:

```
sh <(curl https://j.mp/spf13-vim3 -L)
```

之后脚本就会一步步将spf13安装到vim中，中间有一个插件要求输入github账号密码，不过输入了也装不上 - - 所以不要在意啦。

如果你安装完成，你再在 vim 输入的时候，就会用自动补全，这对于输入路径来说是最方便不过的事情了，尽情享用吧~

