---
title: go语言复合类型笔记 - 1
date: 2018-02-05
tags: 
    - go
categories: 
    - 技术
    - 笔记
thumbnail: https://upload-images.jianshu.io/upload_images/1846712-e9c6418a76b37d13.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240
---


**摘要**


![](https://upload-images.jianshu.io/upload_images/1846712-e9c6418a76b37d13.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

为了学习云，从 Java 的坑里，挪了半只脚去了 Go 语言，感觉 Go 是一个 C 和 Python 的孩子。此篇是记录 Go 语言圣经中第四章复合类型重点内容的一篇笔记，Go 还是有很多很有意思、与 Java 不同的地方，包括了数组、切片、Map等。

<!--more-->

# 数组


数组是一个固定长度的序列。在 Go 语言中，很少直接使用数组，一般会使用与数组类型对应的动态类型 `Slice`。类似于 Java 中的 `ArrayList`，可以动态的增长和收缩序列，不过还是从数组开始。


``` go
var a [3]int            // 3个int类型元素的数组
fmt.Println(a[0])       // 输出第一个元素
fmt.Println(a[len(a)-1]) // 输出最后一个元素

// 循环打印所有的元素 的 Index 和 value
for i, v := range a {
    fmt.Printf("%d %d\n", i, v)
}

// 循环打印所有元素的 value
for _, v := range a {
    fmt.Printf("%d\n", v)
}

```

看起来和 Java 的数组是相同的，包括初始化的过程，比如标定大小和使用花括号，只是 Go 使用 `var` 来推断类型，并且不需要 `new` 关键词来声名。

``` go
var q [3]int = [3]int{1, 2, 3}
```

在数组中，默认的初始值都是零值。对于不同的类型来说，零值也不同，不过int 类型当然就是0。

不过 Go 语言的数组有一些奇怪的特性，例如：

**指定参数的初始化**

``` go
type Currency int

const (
    USD Currency = iota // 美元
    EUR     // 欧元
    GBP     // 英镑
    RMB     // 人民币
)

symbol := [...]string{USD: "$", EUR: "€", GBP: "￡", RMB: "￥"}

fmt.Println(RMB, symbol[RMB])   // "3 ￥"
```

上述的初始化数组的过程中，使用 `...` 来通过元素个数指定长度，而索引则根据开发人员指定的字面量来存放，看起来和 `map` 非常像，在这种初始化的方式中，key 的顺序是无关紧要的，未指定初始值的元素将用零值初始化。这和 Js 中的数组或者说对象是类似的。访问的时候可以通过中括号来访问。



另外一个与 Java 不同的地方在于，Java 中的数组类型已经是一个引用类型，其实例是一种对象。但是，对于 Go 来说，传参时仍然是复制了一份数据进行传输，非常的低效。所以 Go 语言中也存在 c 中的指针，只不过是一种更轻松的指针。

``` go
func zero(ptr *[32]byte) {
    for i := range ptr {
        ptr[i] = 0
    }
}
```

我们可以显式地传入一个数组指针，那样的话函数通过指针对数组的任何修改都可以 直接反馈到调用者

当将一个数组的引用传递给一个方法时，就可以在方法中对数组的值进行修改，使用的时候需要确认方法接受的参数，是不是一个指针类型 如 `*[32]byte` 就是一个指针类型的形参，你可以使用 `ptr[0]` 来获取数组中的数据。


# Slice

由于数组的不灵活，`Slice` 才是真正经常使用替代数组的类型。它的语法和数组很像，只是没有固定的长度而已。可以通过内置的方法 `len` 和 `cap` 函数分别返回 slice 的长度和容量。

## 切片

切片的操作 `s[i:j]` 表示，引用 s 的第 i 个元素开始到第 j-1 个元素的子序列来生成一个新的slice。

在 Python 中也有类似于切片的概念，Js也有，看起来这种和 Matlab 投影操作很像的方式已经开始被新语言广泛使用。但是具体使用的时候，需要注意的一点是，对于 Go 语言的 slice 操作来说，**切片是一种引用的操作**。

``` go
var first = []int{1,2,3,4, 5}
var second = first[:3]
fmt.Println(first)
fmt.Println(second)
second[0]= 3
fmt.Println(first)   // 3
```

上面代码对 `second` 的修改，事实上也是对 `first` 的修改。

``` go
months := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}
q2 := months[4:7]
summer := months[6:9]
fmt.Println(q2)
fmt.Println(summer)

fmt.Println("重叠的月份")
for _, v := range summer {
	for _, q := range q2 {
		if v == q {
			fmt.Printf("%d appears in both \n", q)
			// 7
		}
	}
}

fmt.Printf("number 的长度为 %d\n", len(months))			// 12
fmt.Printf("summer 的长度为 %d\n", len(summer))			// 3
fmt.Printf("summer 的长度为 %d\n", cap(summer))		// 6

// 可以超出len的长度去获取数据，但是不可以超出cap
fmt.Println(summer[:5])		// [7 8 9 10 11]
```

需要注意的是，对于字符串类型和 byte[] 类型的切片操作会生成一个新的字符串或者 byte[]。




