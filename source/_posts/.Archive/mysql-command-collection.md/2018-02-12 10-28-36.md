---
title: 文章标题
date: 2018-01-01
tags: 
    - Java
categories: 
    - 技术
---

# 安装

安装的过程非常机械，重点在于
[Ubuntu 16.04 上安装 MySQL 5.7 教程](http://www.linuxidc.com/Linux/2017-05/143864.htm)

[CentOS7 64位下MySQL5.7安装与配置](http://www.linuxidc.com/Linux/2016-09/135288.htm)


# 配置

### 获取默认密码

```
grep 'temporary password' /var/log/mysqld.log
```

### 开放远程访问

```
sudo vi /etc/mysql/mysql.conf.d/mysqld.cnf
```
找到
`bind-address            = 127.0.0.1`

前面加#注释掉，`wq!`强制保存并退出，重启服务。

***

# 创建用户

```
//创建用户
CREATE USER 'username'@'host' IDENTIFIED BY 'password'; 
//授权远程访问
GRANT ALL PRIVILEGES ON *.* TO 'username'@'%' IDENTIFIED BY 'password' WITH GRANT OPTION;
//修改默认密码
ALTER USER 'root'@'localhost' IDENTIFIED BY 'password'; 
//修改密码
set password for 'root'@'localhost'=password('password'); 
```

***

# 创建库

```
create database name character set utf8 collate utf8_general_ci;
```

***

# 重启服务

ubuntu下

```
service mysql restart
```

centos下
```
//启动服务
systemctl start mysqld
//开机启动
systemctl enable mysqld
systemctl daemon-reload
```

***

#连接时指定时区/编码

```
//设置时区
&serverTimezone=UTC
//设置编码
&characterEncoding=utf-8
//自动重连
&autoReconnect=true
```