---
title: Leetcode常用代码段
date: 2017-09-06
updated: 2017-09-06
categories: 
    - 技术
    - 代码段
tags: 
    - leetcode
thumbnail: https://upload-images.jianshu.io/upload_images/1846712-21962d192e8b9069.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240
---

**摘要**


leetcode已经成为面试必备，为了保证刷完不小心（常态 忘掉之后，能够快速的回忆起来，因此找个地方记下来。

![](https://upload-images.jianshu.io/upload_images/1846712-21962d192e8b9069.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240
)

<!-- more -->


# 位运算

## 进制转换

### 190 翻转二进制

[Leetcode190](https://leetcode.com/problems/reverse-bits/discuss)


``` java
// 输入一个整数，转换成二进制，逆序，再转换成十进制。
public int reverseBits(int n) {
    if (n == 0) return 0;
    
    int result = 0;
    for (int i = 0; i < 32; i++) {
        result <<= 1;
        if ((n & 1) == 1) result++;
        n >>= 1;
    }
    return result;
}
```

---


马拉车算法


