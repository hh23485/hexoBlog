---
title: tmux 使用及配置
date: 2018-02-15
tags: 
    - tmux
categories: 
    - 技巧
    - 效率
style: post
---

根据 简书博客 [tmux简洁教程及config关键配置](https://www.jianshu.com/p/fd3bbdba9dc9) 和 伯乐在线 [Tmux 速成教程：技巧和调整](http://blog.jobbole.com/87584/) 的两篇博客，我也用上了比较好用的 tmux.

贴一下 tmux 的配置和快捷键作用，以及自己配置的 alias。

<!--more-->

# tmux 配置

配置文件 `~/.tmux.conf` 内容如下：

```
# ignore
unbind C-b
set -g prefix C-b

# 设置按键为 vim 按键
setw -g mode-keys vi

# 使用 alt + 方向键切换 panes
bind -n M-Left select-pane -L
bind -n M-Right select-pane -R
bind -n M-Up select-pane -U
bind -n M-Down select-pane -D

# 使用 shift + 方向键切换 window
bind -n S-Left previous-window
bind -n S-Right next-window

# 通过 prefix + r 直接更新配置
bind-key r source-file ~/.tmux.conf \; display-message "tmux.conf reloaded"

# 使用 prefix + h 水平切分窗口
bind-key v split-window -h

# 使用 prefix + v 垂直切分窗口
bind-key h split-window -v

# 设置底栏
# 颜色
set -g status-bg black
set -g status-fg white

# 对齐方式
set-option -g status-justify centre

# 左下角
set-option -g status-left '#[bg=black,fg=green][#[fg=cyan]#S#[fg=green]]'
set-option -g status-left-length 20

# 窗口列表，自动以当前文件夹命名 window
setw -g automatic-rename on
set-window-option -g window-status-format '#[dim]#I:#[default]#W#[fg=grey,dim]'
set-window-option -g window-status-current-format '#[fg=cyan,bold]#I#[fg=blue]:#[fg=cyan]#W#[fg=dim]'

# 右下角
set -g status-right '#[fg=green][#[fg=cyan]%m-%d %H:%M#[fg=green]]'
```

![image.png](https://upload-images.jianshu.io/upload_images/1846712-9c34d81ec479822d.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


在整个配置过程中有几个比较增加幸福感的部分：

- 使用 `alt + 方向键` 切换 panel
- 使用 `shift + 方向键` 切换 window
- 使用 `prefix + v` 垂直拆分窗口
- 使用 `prefix + h` 水平拆分窗口
- 自动使用目录路径命名窗口

# alias 配置

同时，对于 tmux 来说，有些命令太长了，在你的 sh 配置一个快捷键或者 alias 是非常提升效率的。

``` bash
alias tm="tmux"
alias tma="tmux attach"
alias tmt="tmux attach-session -t"
alias tmk="tmux kill-session -t"
```

我在 `~/.zshrc` 中定义了 4 个 alias:

1. `tm` 用于快速启动 tmux
2. `tma` 用于快速连接到默认的 tmux 会话
3. `tmt` 用于直接连接指定的 tmux 会话
4. `tmk` 用于 kill 一个会话

之后，在使用的过程中，基本上连上远程之后，输入 `tma` 就能够继续工作~




