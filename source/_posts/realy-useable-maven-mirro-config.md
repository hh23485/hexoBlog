---
title: 一个可用的阿里云 maven 和 gradle 镜像配置
date: 2018-01-01
tags: 
    - maven
    - gradle
    - mirror
categories: 
    - 技术
    - 配置
---

**摘要**

`maven` 的中央仓库服务器在国外, 对于网络环境不是很好的中国开发者来说,下载并编译可能会耽误非常多的时间, 因此, 阿里云的镜像就能够带来飞速的感觉。这篇是一个可用的 `maven` 镜像配置，以及对应的 `gradle` 配置。

<!--more-->

# maven

首先, 如果你使用maven来管理依赖, 你需要做如下配置:

Maven的默认Setting文件默认放在`~/.m2/`文件夹内的`setting.xml`文件。只需要把该内容替换到文件中，即可以使用aliyun的镜像来下载maven的依赖了，亲测可用。

不过，如果你已经为maven配置了一些参数，那你直接将`<mirror>`的内容添加到原有配置中即可。

所有的改动请先提前备份，确认新配置可用后再删除备份。

``` xml
<?xml version="1.0" encoding="UTF-8"?>
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">
  <mirrors>
    <mirror>
      <id>alimaven</id>
      <name>aliyun maven</name>
      <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
      <mirrorOf>central</mirrorOf>        
    </mirror>
  </mirrors>
</settings>
```


# gradle

不过, 如果你使用 **gradle** 的话, 你需要在 `.gradle` 文件夹下面的 `init.gradle` 文件(没有则自己创建)中添加如下部分内容:

``` gradle
allprojects{
    repositories {
        def REPOSITORY_URL = 'http://maven.aliyun.com/nexus/content/groups/public/'
        all { ArtifactRepository repo ->
            if(repo instanceof MavenArtifactRepository){
                def url = repo.url.toString()
                if (url.startsWith('https://repo1.maven.org/maven2') || url.startsWith('https://jcenter.bintray.com/')
                ||url.startsWith('https://repo.spring.io/plugins-release') ||url.startsWith('https://dl.bintray.com/kotlin/kotlin-eap-1.1')){
                    project.logger.lifecycle "Repository ${repo.url} replaced by $REPOSITORY_URL."
                    remove repo
                }
            }
        }


        maven {
            url REPOSITORY_URL
        }
    }
}
```

