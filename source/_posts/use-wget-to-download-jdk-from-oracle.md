---
title: 解决官网下载 jdk 只有 5k 大小的错误
date: 2018-02-13 20:48:46
tags: 
    - ubuntu
    - jdk
    - 遇坑
---

你有没有遇到过，你用 `wget` 在 Oracle 官网给你的服务器下载 `jdk` 或者 `jre` 时，看起来一切正常，下载下来却是 5k 大小的同名文件?

我找到解决办法了😃 

<!-- more -->

以下内容最初来源于博客，但我抽取并翻译了原因和解决方案的核心部分。

[Oracle / Sun Java – JRE/JDK Download Using Wget](http://blog.heeresonline.com/2014/10/oracle-sun-java-download-using-wget/)


# 下载 JDK

虽然网上很多人提供了各个版本 JDK 和 JRE，特别是 CSDN 这类资源站，文不对题，层次不齐，还要积分，这就非常不开心了。所以，安装依赖库必须去官网无疑，毕竟 Linux 下安装依赖本来就很简单了。

所以打开 [ORACLE JDK](http://www.oracle.com/technetwork/java/javase/overview/index.html) 官网下载 JDK，选择了版本，获取了下载之后，浏览器就开始下载。

到这里都没有问题，可是我们在服务器上，并不想要通过 sftp 再传上去，太麻烦了，所以需要直接在终端里面下载。于是，我们拷贝一下下载链接：

``` bash
http://download.oracle.com/otn-pub/java/jdk/8u161-b12/2f38c3b165be4555a1fa6e98c45e0808/jdk-8u161-linux-x64.tar.gz
```

然后去命令行里输入：

``` bash
wget http://download.oracle.com/otn-pub/java/jdk/8u161-b12/2f38c3b165be4555a1fa6e98c45e0808/jdk-8u161-linux-x64.tar.gz
```

如图所示，就像想象的那样，它会下载，然后解压，设置路径，完成。。。

![image.png](https://upload-images.jianshu.io/upload_images/1846712-9aefbcbe9fe2efe2.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/800)

但是。。

![image.png](https://upload-images.jianshu.io/upload_images/1846712-4e379a9c31acee9f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

当你下载完成你就发现，飞快的进度条里显示，下载完成的文件只有 5k 大小。

# 原因

在下载 jdk 的时候，事实上都会注意到一个小细节，就是你需要 **Accept License Agreement**，接受它的证书。

![image.png](https://upload-images.jianshu.io/upload_images/1846712-55185cdb6de7fdf1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

许多的其他网站里，接受许可，只是用来显示下一步的按钮，但是这里做了更多的操作。

``` html
<form name="agreementFormjdk-8u162-oth-JPR" method="post" action="radio" class="lic_form">
  <input type="radio" value="on" name="agreementjdk-8u162-oth-JPR" onclick="acceptAgreement(window.self, 'jdk-8u162-oth-JPR');"> &nbsp;Accept License Agreement&nbsp;&nbsp;&nbsp; 
  <input type="radio" value="on" name="agreementjdk-8u162-oth-JPR" onclick="declineAgreement(window.self, 'jdk-8u162-oth-JPR');" checked="checked"> &nbsp; Decline License Agreement
</form>
```

可以看到在点击 **Accept** 的时候，会触发一个 js 的方法 `acceptAgreement`

在这个方法中，做了如下的事情：

``` javascript
function acceptAgreement(windowRef, part){
  var doc = windowRef.document;
  disableDownloadAnchors(doc, false, part);
  hideAgreementDiv(doc, part);
  writeSessionCookie( 'oraclelicense', 'accept-securebackup-cookie' );
}
```

设置了一个会话 cookie，key是 `oraclelicense`，值是 `accept-securebackup-cookie`，这在直接使用 wget 的时候，是没有设置的。

# 解决方案

所以在下载的时候，wget 命令中也需要加入这个 cookie 值，命令如下即可：

``` bash
wget --header='Cookie: oraclelicense=accept-securebackup-cookie' http://download.oracle.com/otn-pub/java/jdk/8u161-b12/2f38c3b165be4555a1fa6e98c45e0808/jdk-8u161-linux-x64.tar.gz
```


