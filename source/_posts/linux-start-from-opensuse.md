---
title: Hello Linux World
date: 2017-08-14
categories: 
    - 技术
    - 配置
tags: 
    - linux
    - openSUSE
---

<!--more-->


终于，摆脱了windows，去了linux。

# 前言
已经学了5年的计算机了，都没有正儿八经的用一用linux。在开始读研的时候，跟大我几届的朋友出去吃饭，意外的被问到linux操作熟不熟，然后收到了一顿鄙视。

所以大概不懂得linux操作的人，应用技术的场景非常非常少，更别谈分布式之类的技术了。毕竟，整个后端都要在linux上奔跑才行。

事实上，我已经用了接近两年的mac，只不过在强有力的GUI支持下，一直都只是以一种“更好看”的系统的体验在使用。

在一段时间的积累下，今天开始把实验室的主力机更换成linux了。

***

## 另外一种双系统

第一个遇到的问题，毫无疑问就是安装。
在华为项目的**强力驱动**下，在我的可怜的电脑里，始终要存放一个Windows的环境，进而，问题就出现了。怎么去安装一个双系统。

一般来说，双系统有两种形式：

1. 虚拟机
2. 单硬盘双系统
3. 双硬盘双系统

虚拟机就不用多少了，基本上快速安装一下就好。而单硬盘双系统也是如此，网上有一大堆教程，当你安装的时候，只需要考虑如何分区，然后把引导建在/boot下即可。

现在问题是，我的电脑是两块硬盘，一块windows，而另外一块希望做成Linux，这非常的尴尬。因为我之前尝试过很多次，将引导放在/boot下是没有办法引导起来的（就是网上那些easy bcd引导的教程）。最终，这次直接将引导挂在了整块磁盘上，终于成功了。

事实上逻辑是这样，

首先，我选择了一个叫做openSUSE的系统，他的logo是一个绿色的蜥蜴🦎，号称自动配置双系统，这是我这次尝试的主要动力。

其次，我的电脑是不支持UEFI启动了老电脑 - - ，在安装的过程中，不要通过UEFI启动U盘，然后进去安装系统，没有要求配置/boot，想必是自动配置，然后配置一下用户信息，就可以安装完成。

后来，我尝试了一下这样配置Linux，发现如果是UEFI的ubuntu，在安装的时候挂载好/boot/efi之后，将引导直接挂载整个第二块磁盘上，就可以在启动菜单里选择硬盘启动进行引导，不过很明显没有能够找到win的引导。不过问题不大，因为重新进入引导菜单选择win的硬盘启动就可以了。


***

# 配置

事实上，在整个安装的过程中，我装了四五种linux发行版，但都不太好使，特别是ubuntu，3分钟一个错误弹窗。。。不知道是不是电脑的问题，还是本身就是这样 - -

在openSUSE的使用过程中，几乎没有异常弹窗，只有一次提示了硬盘空间不足，所以建议新手想要尝试linux的时候，可以从openSUSE开始入门。

## 分区

分区是最害怕的一件事情，因为随时可能会导致数据丢失，好在在linux安装的过程中，会先进行预览分区，一直到正式安装之前都不会格式化硬盘，还会一直询问你是不是真的确认要这么干。在openSUSE中更直白一点：`如果你不知道你在做什么的话，请不要继续`。。。

在分区的过程中，遇到过两个问题：

1. gpt标签问题

这个问题在我从非UEFI引导的启动菜单中进入的时候，就没有这个提示了。 
我的意思是：在我的电脑上，插上优盘启动后，会看见两个U盘启动，一个是UEFI开头的，一个是普通的storage。从storage进入之后，不会要求修改硬盘标签。

2. 分区大小

这次感受了一下Linux的分区，主要的分区大概分这么几个

- /boot
- swap
- /home
- /
- /usr
- /var

这是几个占得比较大的部分。在使用的过程中，我的电脑出现过一次错误提示说硬盘空间不足，发现/目录下，也就是系统目录空间不足。

我在安装系统的时候，给 `/`分配了16G，给`/home`分配了60G，给`swap`分配了16G，其他都是默认配置，在这种情况下安装完成后，`/`仅有8G的大小，想来是系统接管了一部分。 网上有一些lvm下调整空间的方案，但是我一个小白还不太能理解前言后语，因此我直接重装了系统，给了`/`32G。

>其实问题在于，网上很多教程告诉用户，`/`分配10G就可以了。。。难道这些人不开发的么，maven、docker、jdk、git、nginx还有很多日志都是默认在`/`下面，特别是直接用包管理工具安装的应用。这么点空间让用户怎么玩。。。
>以后还是搞一个50G的系统盘，配上一个200G的`/home`来使用吧。

***

## Gnome

Gnome是一个桌面环境，在Linux中桌面环境与内核是分开的，可以一套内核使用多种桌面。貌似一般Linux环境下都提供了两个基础的桌面分别是KDE和Gnome，ubuntu之前用的是unity，不过最近宣称也要换成Gnome了。

Gnome在一定程度上感觉像极了mac，虽然软件质量讲真不是一个量级，但是起码从windows的传统样式中脱离了出来。采用了类似launchpad的启动器，和比windows搜索快得多的搜索方式。

不过这里要说的是两个方面，一个是gnome的主题，一个是gnome的插件。



### 主题

其实我还没有开始使用主题，这块后台再补。

### 插件

下面是一些主要的插件：

1. [Drop Down Terminal](https://extensions.gnome.org/extension/442/drop-down-terminal/) 一个下拉触发的终端非常好用，快捷键是tab上面那个键，这个快捷键的说明看了好多遍才理解 - -
2. [Clipboard Indicator](https://extensions.gnome.org/extension/779/clipboard-indicator/) 一个简单的剪切板管理，快捷键是Ctrl + F9
3. [NetSpeed](https://extensions.gnome.org/extension/104/netspeed/) 一个在状态栏显示网速的插件
4. [Dash to dock](https://extensions.gnome.org/extension/307/dash-to-dock/) 固定dock
5. [Places Status Indicator](https://extensions.gnome.org/extension/8/places-status-indicator/) 一个快捷的访问home/图片等等路径的下拉菜单
6. [Wikipedia Search Provider](https://extensions.gnome.org/extension/512/wikipedia-search-provider/) 在本地搜索时生成wiki
7. [TopIcons](https://extensions.gnome.org/extension/495/topicons/) 将托盘图标放到状态栏上
8. [Recent Items](https://extensions.gnome.org/extension/72/recent-items/) 在状态栏显示最近访问的文件

***


# 安装应用

对于安装应用来说，openSUSE有一个非常酷的网站叫做 [PackageSearch](https://software.opensuse.org/find)，上面登记了非常多的应用和扩展，可以直接搜索获得结果，并通过提供的一键安装直接装到系统中。


## 日常

从日常开始，日常基本上在做的事情大约是文档、markdown、视频、pdf、词典、截图、聊天、音乐。

基本上作为一个开发机，拥有这些内容就足够了，下面简单介绍一下各个部分的内容。


### 视频

#### flash

虽然在mac上flash呗唾弃的不成样子，但是离开了flash在国内的视频网站上也是活不下去的。之前的`妈妈再也不用担心发烫`计划好像现在也失效了很多很多。

但是flash的安装非常的容易，只需要在嵌入flash的视频播放页面点击安装flash就可以跳转到flash的安装页面，下载并安装即可。然后去播放页一刷新。

***


### 聊天

#### 微信

在windows的世界里，装一个聊天软件简直容易。在linux下面便得非常的难，ubuntu之类的应用商店里有的提供了一些qq或者微信，但在大部分系统里还是要自己折腾。

在openSUSE中，有一个开源项目[electronic wechat](https://github.com/geeeeeeeeek/electronic-wechat)，提供了在linux上封装的web版本wechat。

同样的，解压完成就可以运行。在确定能够运行后，可以在`/usr/share/applications`文件夹内添加一个wechat.desktop。这是Gnome生成桌面应用的方式，符合标准即可。


``` desktop
[Desktop Entry]
Name=Wechat
Exec=替换成wechat运行的路径
Comment=Wechat
Icon=替换成wechat的logo的路径->自己下载一个微信的png放在运行同文件夹内比较好
Type=Application
Terminal=false
Encoding=UTF-8
```

编辑和创建都需要sudo权限的支持，创建完成后即可在搜索内搜到并打开。可能存在部分打开出现异常的情况，重启就好。



#### 邮件

to be continue...

***


### 音乐

#### 宇宙第一播放器 网易云音乐

网易云音乐官网有deb格式的包，用来给ubuntu安装，但是显然openSUSE这类使用rpm安装的没有被照顾，不过好在也有办法来处理。不过稍微麻烦一点。

1. 在openSUSE的[软件搜索](https://software.opensuse.org/find)页面搜索`netease-cloud-music`，可以得到网易云音乐的包，点进去进行一键安装，可以获得一个网易云音乐的脚本和源地址。但事实上这个脚本在后台的执行是失败了的。
2. 在终端中删除刚才安装的库，`sudo zypper rm netease-cloud-music`，这个时候源还保留着。
3. 进去[网易云官网](music.163.com/#/download)，下载ubuntu16.04(64bit)的deb包，重命名为`netease-cloud-music_1.0.0_amd64_ubuntu16.04.deb`，放在`/tmp`文件夹下。
4. 更新个库`sudo zypper in binutils`
5. 再次安装网易云`sudo zypper in netease-cloud-music`
6. 这个时候就可以正常的安装完成了，可以在launchpad中查看网易云音乐

原理是这样的，在源中下载的应该是一个脚本，对deb进行拆包。脚本里面自动会下载一个deb版本的包，但是不知道处于什么原因，可能下载现在已经不能成功了。因此需要自己放一个deb的包在`/tmp`下，再次运行脚本，就成功了。

如果你在第3步重新安装网易云，就会看到找不到deb文件的错误，解决方案就是后续步骤。



***

## 开发

开发我们一点点来.

### 首先从git

git官方的文档里显示，不太需要直接下载源码编译，直接从zypper下载即可

``` shell
sudo zypper in git
```

一顿通过之后，就可以下载完成直接使用。


***


### 然后是jdk

#### 卸载openJDK

我是一个java程序员。。。jdk毫无疑问必不可少，对于系统自带的openJDK来说，还是选择oracle的jdk更放心一些。

所以首先，需要卸载原有的openJDK，根据[教程](www.mamicode.com/info-detail-184869)的说法，只需要通过rpm查找到所有的包，然后删除他们即可。

我发现在openSUSE中，可以直接通过zypper删除所有openjdk的组件也可以完成该需求，步骤如下：


首先，输入

``` shell
java -version
```

查看当前安装的java的版本，在我的电脑上，默认是这样的：


然后输入删除命令

``` shell
sudo zypper rm java-1_8_0-*
```

这样就一次性删除了所有的openJDK相关的包，之后通过[官方](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)的jdk安装回来即可。

#### 安装oracle jdk

对于openSUSE的系统来说，需要下载`Linux64 - rpm`版本，双击安装就可以实时生效。

安装完成后，在终端输入`java -version`，发现已经变成刚才安装的版本。

这个时候需要简单的配置一下`JAVA_HOME`环境变量，来保证maven等程序可以正常的运行，步骤如下：

1. 输入 `vim ~/.bashrc`，这是bash在当前用户下的配置文件
2. 使用vim编辑器，在文档中输入`export JAVA_HOME=/usr/java/jdk1.8.0_144`
3. 在下一行输入`export PATH=$JAVA_HOME/bin:$PATH`
4. 保存退出
5. 在终端输入`source ~/.bashrc`生效

这个时候可以测试`echo $JAVA_HOME`是否输出为刚才设置的路径。（该路径是rpm模式的jdk安装的位置）


***


### maven

maven对于java程序员来说无比重要，无论从jar包管理还是构建上来说都是非常高效而统一的。对于使用IDEA的开发人员来说，虽然idea自带了一个maven，但貌似时常出问题，因此自己安装一个maven还是非常有必要的。

maven的安装一般有两种，一种是通过下载二进制版本，放在文件夹里，然后配置一下环境变量。还有一种是下载Source源码来编译并安装。

不过既然用了openSUSE，直接从[官方库](https://software.opensuse.org/package/maven)下载也是极好的。运行的前提是安装了`jdk`并配置了`JAVA_HOME`。


***

### tomcat

Tomcat是JavaEE的一个实现，作为一种java web应用奔跑的容器。在Linux里面的安装其实和Windows差不多，[下载](http://mirror.bit.edu.cn/apache/tomcat/tomcat-8/v8.5.20/bin/apache-tomcat-8.5.20.zip)完，解压，放在一个文件夹里，然后讲bin目录配置到环境变量path中。

在Linux中具体的操作如下:

1. 下载，并解压到文件夹中，我为了避免系统空间不足，我将tomcat放置在了`~/lib/tomcat`下。
2. 在`~/.bashrc`文件中添加`export PATH=tomcat目录/bin:$PATH`，注意后面的$PATH一定不能少，不然系统会出错，所有的命令都将找不到。

配置完成后，一般都是通过idea启动的，所以其实没有太大的差别，在idea中再指定一次tomcat位置即可。


***

### idea

Idea的安装其实没有太多的悬念，从[JetBrains官网](https://www.jetbrains.com/)下载下来，解压双击即可运行。

因此，需要在意的是，如何生成一个快捷方式能够在launchpad里面直接点击运行，或者放在dock中。

这里给出一个简单的配置快捷方式的模板：

首先，在`/usr/share/applications`里面创建`idea.desktop`。通过 `sudo vim` 来打开并填入如下信息：

``` desktop
[Desktop Entry]
Name=IntelliJ IDEA
Exec=替换成idea.sh的路径->在idea解压后bin文件夹内
Comment=IntelliJ IDEA
Icon=替换成idea.png的路径->在idea解压后的bin文件夹内
Type=Application
Terminal=false
Encoding=UTF-8
```

其中的路径都是可以从文件夹中直接拖拽到终端中自动生成的。

至于idea的使用，事实上有很多技巧，但是注意事项倒很少。启动以后进行简单的配置，然后指定一下jdk的位置即可。


***

### docker

to be continue...


***

### zsh - 终极shell

zsh是一个非常酷的shell版本，远比已有的bash好得多。

``` shell
sudo zypper in zsh
```

即可安装，不过，zsh的配置比较麻烦，好在有一个叫做oh-my-zsh的项目能够一键配置，在终端输入如下命令即可：

``` shell
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```

然后通过如下命令切换

``` shell
chsh 用户名 -s /bin/zsh
```

重启或注销后就可以使用。之前配置的JAVA_HOME等参数，需要转移到`~/.zshrc`文件中，但是这个是值得的，毕竟zsh要好用太多。

另外，需要配置的是主题，这里有一个推荐的主题模式：

``` shell
vi ~/.oh-my-zsh/themes/robbyrussell.zsh-theme
```

把里面的内容替换为如下内容：

``` shell
local ret_status="%(?:%{$fg_bold[green]%}➜ :%{$fg_bold[red]%}➜ %s)"
#配置

PROMPT='%{$fg_bold[red]%}➜ %{$fg_no_bold[green]%}%p%{$fg[green]%}%d %{$fg_no_bold[cyan]%}$(git_prompt_info)%{$fg_bold[cyan]%}% %{$fg_bold[green]%}> %{$reset_color%}'

#PROMPT='${ret_status}%{$fg_bold[green]%}%p %{$fg[cyan]%}%c %{$fg_bold[blue]%}$(git_prompt_info)%{$fg_bold[blue]%} % %{$reset_color%}'

ZSH_THEME_GIT_PROMPT_PREFIX="git:(%{$fg_bold[red]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg_no_bold[cyan]%}) %{$fg_no_bold[yellow]%}✗ %{$reset_color%}"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg_no_bold[cyan]%}) "
```

同样，注销重启即生效。

zsh好处太多。。。起码忽略大小写简直高效。





