---
title: Mysql 常用命令
date: 2018-01-01
tags: 
    - Java
categories: 
    - 技术
---

**摘要**

授权、默认密码、忘记密码、创建用户、设置开机启动、重启服务、配置远程访问、jdbc的连接里有哪些参数，同样记不住，同样记下来。

<!--more-->

# 安装

安装的过程非常机械，重点在于配置，所以引用了Linux公社的安装文章。

- ubuntu: [Ubuntu 16.04 上安装 MySQL 5.7 教程](http://www.linuxidc.com/Linux/2017-05/143864.htm)

- centos: [CentOS7 64位下MySQL5.7安装与配置](http://www.linuxidc.com/Linux/2016-09/135288.htm)


# 配置

### 获取默认密码

``` shell
grep 'temporary password' /var/log/mysqld.log
```

### 开放远程访问

``` shell
sudo vi /etc/mysql/mysql.conf.d/mysqld.cnf
```

找到

`bind-address            = 127.0.0.1`

前面加#注释掉，`wq!`强制保存并退出，重启服务。

***

# 创建用户

``` sql
//创建用户
CREATE USER 'username'@'host' IDENTIFIED BY 'password'; 
//授权远程访问
GRANT ALL PRIVILEGES ON *.* TO 'username'@'%' IDENTIFIED BY 'password' WITH GRANT OPTION;
//修改默认密码
ALTER USER 'root'@'localhost' IDENTIFIED BY 'password'; 
//修改密码
set password for 'root'@'localhost'=password('password'); 
```

***

# 创建库

``` sql
create database name character set utf8 collate utf8_general_ci;
```

***

# 重启服务

ubuntu下

``` shell
service mysql restart
```

centos下
``` shell
//启动服务
systemctl start mysqld
//开机启动
systemctl enable mysqld
systemctl daemon-reload
```

***

#连接时指定时区/编码

``` shell
//设置时区
&serverTimezone=UTC
//设置编码
&characterEncoding=utf-8
//自动重连
&autoReconnect=true
```

