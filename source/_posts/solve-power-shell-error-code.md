---
title: 解决 ubuntu powerline 乱码
date: 2017-10-18
categories: 
    - 技术
    - 效率工具
tags: 
    - ubuntu
    - 乱码
    - 遇坑
---



zsh中有一个非常漂亮的主题**[agnoster](https://github.com/agnoster/agnoster-zsh-theme)**，这个主题与[powerline](https://github.com/powerline/powerline)有着解不开的关系。

启用之后，终端的外观会变成这样。

![主题样式](https://upload-images.jianshu.io/upload_images/1846712-44389dcae1402a88.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


上面是理论情况，但是发生了一下问题，在我的机器上 ubuntu 16.04 ，我的部分符号出现了乱码，这就非常尴尬了。

<!--more-->

![乱码主题样式](https://upload-images.jianshu.io/upload_images/1846712-cea0d90248383e3f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)


我找了[powerline的字体](https://github.com/powerline/fonts)仓库，里面说道了要patch字体，然后并没有什么用。可以通过输入`echo "\ue0b0 \u00b1 \ue0a0 \u27a6 \u2718 \u26a1 \u2699"`来判断和预期是否相符。

![符号](https://upload-images.jianshu.io/upload_images/1846712-ba1c563c91c5321c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

最后，找到了解决方案来填充了缺失的字体：

``` bash
# 下载字体文件
# 字体文件可以直接使用浏览器下载
wget https://raw.githubusercontent.com/powerline/powerline/develop/font/10-powerline-symbols.conf
wget https://raw.githubusercontent.com/powerline/powerline/develop/font/PowerlineSymbols.otf
# 配置
sudo mkdir /usr/share/fonts/OTF
sudo mv 10-powerline-symbols.conf /etc/fonts/conf.d/
sudo mv PowerlineSymbols.otf /usr/share/fonts/OTF/
```

退出termial，再重新打开即可。



解决方案整理自 [Ubuntu 终端zsh的agnoster主题乱码](http://blog.csdn.net/codermannul/article/details/69802968)

