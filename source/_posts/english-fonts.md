---
title: 英文字体汇总
date: 2018-02-07
tags: 
    - 英文字体
    - 花体
categories: 
    - 美学
---

再好看的字体丢到字体库里也很难快速的找到，那就把第一眼就觉得好看的字体抽出来，以后做  PPT 的封面就不纠结了。

<!-- more -->


# Almyra Script

>[by Alphabeta in Fonts  Script](https://creativemarket.com/habibotang/1703073-Almyra-Script)

比较适合做大面积留白的英文封面

![](https://upload-images.jianshu.io/upload_images/1846712-b2cc79e5b469a876.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![](https://upload-images.jianshu.io/upload_images/1846712-a90e30e18521f5cd.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![](https://upload-images.jianshu.io/upload_images/1846712-f41676d01efd7c5c.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

>Almyra Script:
This sexy font was inspired by my friend, from her eyes until her heart. I create this one from her laugh, smile, and passion. Almyra come with beautiful uppercase and lowercase, stylistic set, alternates, multilingual, etc.


# Andara Font

>[by AQR Typefoundry in Fonts Script](https://creativemarket.com/aqr.typeface/1132365-Andara-Font-%28-30-OFF-%29)

适合放在角落的logo上

![image.png](https://upload-images.jianshu.io/upload_images/1846712-225b7721dcff1ceb.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image.png](https://upload-images.jianshu.io/upload_images/1846712-933f36bf60bb8be1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image.png](https://upload-images.jianshu.io/upload_images/1846712-837f34ea8b2264a1.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image.png](https://upload-images.jianshu.io/upload_images/1846712-d6854e1ff9c1928f.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

>**Andara** is a manual lettering with type soft and sans, These two font script and sans is a great combination for any product and bussines. Andara comfortable for vintage design, classic design, photography, branding design and many more.


# Gineva Script Font

>[by kavoon in Fonts  Script](https://creativemarket.com/kavoon/1062659-Gineva-Script-Font)

适合放在空间大小有限但对比颜色强烈的地方

![](https://upload-images.jianshu.io/upload_images/1846712-22260ab9b5d874ad.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image.png](https://upload-images.jianshu.io/upload_images/1846712-e2414b0b76ed7db5.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image.png](https://upload-images.jianshu.io/upload_images/1846712-86b0a3f9f8c709e7.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

![image.png](https://upload-images.jianshu.io/upload_images/1846712-4af1759784ba80b4.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

