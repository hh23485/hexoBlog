#!/bin/bash

# 部署和推送
hexo clean
hexo g
hexo d


# 提交并推送
git add -A

if [ $1 ]
then
    git commit -am "$1"
else
    git commit -am 'update content'
fi

git push
